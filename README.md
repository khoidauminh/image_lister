# image_lister

Bash script to add images in a directory (of a particular format) to a .lst file. Written to be as small as possible. 


Parameter : full/relative path to directory (not to file).

Optional :

-fps x (default : 24) 

-w x (width of image ; default 1920)

-h x (height of image ; default 1080)

-o path (path to custom file ; the script writes to a list with the same name as of the directory it operates on by default)

-W (overwrite already existing list without asking)


I wrote this script for importing image lists into Cinelerra (video editor), since the software doesn't automatically genertate/detect image sequences.